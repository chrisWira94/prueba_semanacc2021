def numero_factorial(numero):
    if numero!=1:
        return numero*numero_factorial(numero-1)
    return 1

def MCD(num1,num2):    
    residuo = num1%num2
    
    if residuo!=0:
        return MCD(num2,residuo)
    return num2

def MCM(num1,num2):
    mcd = MCD(num1,num2)
    mcm = num1*num2/mcd
    
    return mcm

def termino_fibonacci(numero): #5   0,1,1,2,3
    if numero==1:
        return [0]
    elif numero==2:
        return [0,1]
    else:
        #return termino_fibonacci(numero-1) #4,3        
        lista_terminos = [0,1]
        #Agrega hasta que termino
        for x in range(numero-2): #0,1,2
            termino = lista_terminos[x] + lista_terminos[x+1] #1 |2 | 3
            #print(termino)
            lista_terminos.append(termino) #[0,1,1] | [0,1,1,2] | [0,1,1,2,3]
            #print(lista_terminos)
        
        return lista_terminos

def suma_fibonacci(numero):
    suma=0
    lista_terminos = termino_fibonacci(numero)
    if isinstance(lista_terminos,int):
        if lista_terminos==0:
            suma=0            
        else:
            suma=1
    else:
        for element in lista_terminos:
            suma+=element
    
    return suma

resultado = suma_fibonacci(2)
print(resultado)
